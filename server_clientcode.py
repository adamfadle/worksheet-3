import socket
import struct
import time

def checksum(data):
    """
    Calculate the internet checksum of a bytes object.
    """
    if len(data) % 2:
        data += b'\0'
    s = sum(struct.unpack('!{}H'.format(len(data) // 2), data))
    s = (s >> 16) + (s & 0xffff)
    s += s >> 16
    return ~s & 0xffff

def recv_and_decode_packet(sock):
    """
    Receive and decode a UDP packet from the given socket.
    """
    data, addr = sock.recvfrom(1024)
    source_port, dest_port, length, checksum = struct.unpack('!HHHH', data[:8])
    payload = data[8:].decode()
    return source_port, dest_port, length, checksum, payload

def send_packet(sock, source_port, dest_port, payload):
    """
    Send a UDP packet with the given source and destination ports and payload
    to the given socket.
    """
    length = len(payload)
    checksum_value = checksum(struct.pack('!HH', source_port, dest_port) + b'\0' + payload.encode())
    packet = struct.pack('!HHHH', source_port, dest_port, length, checksum_value) + payload.encode()
    sock.sendto(packet, ('localhost', 12345))

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind the socket to a specific port
sock.bind(('localhost', 12345))

# Receive and decode the welcome message from the client
source_port, dest_port, length, checksum, payload = recv_and_decode_packet(sock)
print('Received packet:')
print('Source Port:', source_port)
print('Dest Port:', dest_port)
print('Data Length:', length)
print('Checksum:', checksum)
print('Payload:', payload)

# Send and receive UDP packets with payload '1111'
while True:
    send_packet(sock, 0, 542, '1111')
    source_port, dest_port, length, checksum, payload = recv_and_decode_packet(sock)
    print('Received packet:')
    print('Source Port:', source_port)
    print('Dest Port:', dest_port)
    print('Data Length:', length)
    print('Checksum:', checksum)
    print('Payload:', payload)
    time.sleep(1)



import socket
import struct
import time

def checksum(data):
    """
    Calculate the internet checksum of a bytes object.
    """
    if len(data) % 2:
        data += b'\0'
    s = sum(struct.unpack('!{}H'.format(len(data) // 2), data))
    s = (s >> 16) + (s & 0xffff)
    s += s >> 16
    return ~s & 0xffff

def recv_packet(sock):
    """
    Receive a UDP packet from the given socket.
    """
    data, addr = sock.recvfrom(1024)
    source_port, dest_port, length, checksum = struct.unpack('!
