def compute_checksum(source_port: int, dest_port: int, payload: bytearray) -> int:
    # combine source port and dest port into a 32-bit value
    pseudo_header = ((source_port & 0xffff) << 16) | (dest_port & 0xffff)

    # if payload length is odd, pad with a zero byte
    if len(payload) % 2 == 1:
        payload += b'\x00'

    # combine each pair of bytes in the payload into 16-bit values and sum them up
    word_sum = sum((payload[i] << 8) + payload[i+1] for i in range(0, len(payload), 2))

    # add the pseudo header value and the UDP length field to the sum
    word_sum += pseudo_header + len(payload)

    # carry out one's complement on the sum and return the result
    checksum = (word_sum & 0xffff) + (word_sum >> 16)
    checksum = ~checksum & 0xffff
    return checksum


def test_compute_checksum():
    source_port = 10
    dest_port = 42
    payload = bytearray(b"Welcome to IoT UDP Server")

    # Calculate expected checksum using the example in the task description
    expected_checksum = 15307

    # Call the function to compute the actual checksum
    actual_checksum = compute_checksum(source_port, dest_port, payload)

    # Compare expected and actual checksums
    assert actual_checksum == expected_checksum
