# Worksheet 3

# Task 1

I included an example of a Python code to implement the basic client, its in the python file 'import_socket.py'. 

I first imported the necessary modules: socket for creating a UDP socket, and base64 for decoding the data received from the server. I created a UDP socket using the socket.socket() function, and bind it to a specific IP address and port number using the bind() method. Then receiving data from the socket using the recvfrom() method, which blocks until it receives data from the server. The 1024 argument specifies the maximum size of the data to be received. Then decode the received data from base64 encoding using the b64decode() method of the base64 module. Then extracted the fields from the decoded data using slicing operations. The first two bytes represent the source port, the next two bytes represent the destination port, the following two bytes represent the data length, and the next two bytes represent the checksum. The rest of the data represents the payload, which I decode from bytes to string using the decode() method with utf-8 encoding. Finally, printed out the extracted fields using the print() function.

**Unit Test**

I wrote a unit test to validate the functionality of the code. I included an example of how to write a unit test for this code using the unittest module, also in the file 'import_socket.py'

The unit test simulates the UDP server by sending a packet to the socket using a fixed format, and then checks if the extracted fields from the received packet match the expected values.

# Task 2

I included one possible implementation of the compute_checksum() function, in the python file 'compute_checksum.py'.

This function first creates a 32-bit pseudo header by combining the source and destination ports. It then checks if the payload length is odd, and pads it with a zero byte if necessary, since the payload needs to be processed in 16-bit (2-byte) chunks. It then combines each pair of bytes in the payload into 16-bit values and sums them up. The pseudo header value and the UDP length field are also added to this sum. Finally, the one's complement of the sum is computed by flipping all bits, and the resulting 16-bit value is returned as the checksum.

The compute_checksum function takes three arguments:

source_port: an integer representing the source port of the UDP packet.
dest_port: an integer representing the destination port of the UDP packet.
payload: a bytearray representing the payload of the UDP packet.

The function first creates a variable called packet and initializes it with the concatenation of the 16-bit source port, 16-bit destination port, 16-bit length (calculated based on the length of the payload), and the payload itself. The length is calculated by adding 8 (the total length of the headers) to the length of the payload in bytes, and then converting the result to a 16-bit value. Next, the function initializes a variable called checksum with the value 0. It then loops through the packet, adding together each pair of bytes and shifting the result right by 16 bits if the sum is greater than 65535 (the maximum value for a 16-bit integer). This is done to ensure that the addition doesn't result in a value that is greater than 16 bits. After the loop, the function adds the one's complement of the checksum to the result of performing a bitwise AND operation on the checksum and 65535 (which results in the checksum with the 17th bit removed). Finally, it returns the one's complement of this result, which is the checksum for the packet.

**Unit Test** 

I included an example unit test for the compute_checksum() function, also in the python file "compute_checksum.py'

This unit test verifies that the compute_checksum() function correctly computes the checksum for the given source port, destination port, and payload. It uses the example provided in the task description to calculate the expected checksum, and then checks that the actual checksum returned by the function matches the expected checksum.

# Task 3

First I have to modify both the client and the server code. These are the changes I had to make:

Modify the server to handle UDP packets instead of TCP packets.
Implement a function in the server that can receive and decode UDP packets.
Modify the client to send UDP packets instead of WebSocket messages.
Implement a loop in the client that sends a UDP packet with payload '1111' to the server every second and receives the response.

I included the server and client code in the python file 'server_clientcode.py'

The first code is a Python script that implements a simple UDP server. The code starts by importing the necessary modules: socket, struct, and sys. It then defines a variable PACKET_FORMAT which is a string representing the format of the UDP packet that the server will be receiving. This format string specifies that the packet has two 16-bit fields for the source and destination ports, a 16-bit field for the length of the packet, a 16-bit field for the checksum, and a variable length field for the payload. Next, the code creates a UDP socket, binds it to a specific port on the local machine, and enters an infinite loop. In each iteration of the loop, the code waits to receive a packet on the socket, unpacks the packet according to the PACKET_FORMAT string, calculates the checksum of the packet, and prints out the contents of the packet.

The second code is a modification of the first code to include a connection to a WebSocket server using the websockets module. After importing the necessary modules, the code defines the WebSocket URI and enters an infinite loop. In each iteration of the loop, the code connects to the WebSocket server, sends a message to the server to indicate that it has connected, and receives and decodes a response from the server. It then enters another infinite loop where it sends a UDP packet to the server with a payload of '1111', waits for a response from the server, and prints out the contents of the packet. The payload of the response packet should contain the current Coordinated Universal Time (UTC). The code then sleeps for one second before repeating the loop.

**Unit Test**

I provided a unit test for the codes to test the functionality of the code. I included a unit test for the client code and server code in the python file 'server_clientunittest.py'

Unit Test for Client Code:

The unit test for the client code tests whether the send_packet() and recv_and_decode_packet() functions work as expected. It first creates a mock websocket object using the unittest.mock library. Then, it creates a client object using the mock websocket, and sends a packet to the server with a specific payload. It then asserts that the packet received from the server matches the expected response. This tests whether the client can correctly send and receive UDP packets to and from the server.

Unit Test for Server Code:

The unit test for the server code tests whether the handle_packet() function works as expected. It first creates a mock socket object using the unittest.mock library. Then, it creates a server object using the mock socket, and sends a mock packet to the server. It then asserts that the response from the handle_packet() function matches the expected response. This tests whether the server can correctly handle incoming UDP packets and generate the correct response packets.










