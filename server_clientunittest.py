import asyncio
import unittest
import time
import websockets

class TestClient(unittest.TestCase):
    
    async def test_client(self):
        uri = "ws://localhost:8765"
        
        async with websockets.connect(uri) as websocket:
            # Receive and decode welcome message
            welcome_message = await websocket.recv()
            print(f"Received welcome message: {welcome_message}")
            
            # Send packet and receive response repeatedly
            while True:
                # Construct packet
                source_port = 0
                dest_port = 542
                payload = '1111'.encode('utf-8')
                data_length = len(payload)
                checksum = 0
                
                # Construct header as bytes
                header = (source_port.to_bytes(2, byteorder='little') +
                          dest_port.to_bytes(2, byteorder='little') +
                          data_length.to_bytes(4, byteorder='little') +
                          checksum.to_bytes(2, byteorder='little'))
                
                # Construct message as bytes
                message = header + payload
                
                # Calculate checksum
                checksum = sum(message)
                checksum = (checksum & 0xFFFF) + (checksum >> 16)
                checksum = ~checksum & 0xFFFF
                
                # Update checksum in header
                header = (source_port.to_bytes(2, byteorder='little') +
                          dest_port.to_bytes(2, byteorder='little') +
                          data_length.to_bytes(4, byteorder='little') +
                          checksum.to_bytes(2, byteorder='little'))
                
                # Construct final message
                message = header + payload
                
                # Send packet
                await websocket.send(message)
                
                # Receive and decode response
                response = await websocket.recv()
                print(f"Received response: {response}")
                decoded_response = decode_packet(response)
                print(f"Decoded response: {decoded_response}")
                
                # Sleep for 1 second
                time.sleep(1)
    
if __name__ == '__main__':
    asyncio.run(TestClient().test_client())


import asyncio
import unittest
import websockets
import random
import struct
import datetime

async def recv_and_decode_packet(websocket):
    # receive and decode packet header
    header = await websocket.recv()
    source_port, dest_port, length, checksum = struct.unpack('<HHHH', header)
    # receive and decode packet payload
    payload = await websocket.recv()
    # calculate checksum of received packet
    packet = struct.pack('<HHHH', source_port, dest_port, length, 0) + payload
    calculated_checksum = calculate_checksum(packet)
    # check if calculated checksum matches received checksum
    if calculated_checksum != checksum:
        raise ValueError('Invalid checksum')
    # decode payload
    decoded_payload = payload.decode()
    # return decoded payload
    return decoded_payload

async def send_packet(websocket, source_port, dest_port, payload):
    # encode payload
    encoded_payload = payload.encode()
    # create packet header
    length = len(encoded_payload)
    packet = struct.pack('<HHHH', source_port, dest_port, length, 0) + encoded_payload
    # calculate checksum
    checksum = calculate_checksum(packet)
    # add checksum to packet header
    packet_with_checksum = struct.pack('<HHHH', source_port, dest_port, length, checksum) + encoded_payload
    # send packet
    await websocket.send(packet_with_checksum)

def calculate_checksum(data):
    checksum = 0
    # iterate over 16-bit words in data
    for i in range(0, len(data), 2):
        word = (data[i] << 8) + data[i+1]
        checksum += word
    # fold 32-bit checksum to 16 bits
    while checksum >> 16:
        checksum = (checksum >> 16) + (checksum & 0xFFFF)
    return ~checksum & 0xFFFF

async def server(websocket, path):
    print('Connected to client')
    await send_packet(websocket, 0, 542, 'Welcome to the server!')
    while True:
        payload = await recv_and_decode_packet(websocket)
        if payload == '1111':
            # get current UTC time and format as string
            utc_time = datetime.datetime.utcnow().strftime('%H:%M:%S').encode()
            # send payload with current UTC time
            await send_packet(websocket, 0, 542, utc_time)

class TestServer(unittest.IsolatedAsyncioTestCase):
    async def test_server(self):
        async with websockets.serve(server, 'localhost', 8765):
            async with websockets.connect('ws://localhost:8765') as websocket:
                # receive welcome message from server
                welcome_message = await recv_and_decode_packet(websocket)
                self.assertEqual(welcome_message, 'Welcome to the server!')
                # send '1111' payload to server and receive UTC time
                await send_packet(websocket, 0, 542, '1111')
                utc_time = await recv_and_decode_packet(websocket)
                # check if received payload is a valid time in format HH:MM:SS
                try:
                    datetime.datetime.strptime(utc_time, '%H:%M:%S')
                except ValueError:
                    self.fail('Invalid UTC time format')
