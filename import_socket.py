import socket
import base64

# create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# bind the socket to a specific IP address and port number
server_address = ('localhost', 1234)
sock.bind(server_address)

# receive data from the socket
data, addr = sock.recvfrom(1024)

# decode the received data from base64 encoding
decoded_data = base64.b64decode(data)

# extract the fields from the decoded data
src_port = decoded_data[0:2]
dest_port = decoded_data[2:4]
data_len = decoded_data[4:6]
checksum = decoded_data[6:8]
payload = decoded_data[8:].decode('utf-8')

# print out the extracted fields
print("Base64: ", data)
print("Server Sent: ", decoded_data)
print("Decoded Packet:")
print("Source Port:", int.from_bytes(src_port, byteorder='big'))
print("Dest Port:", int.from_bytes(dest_port, byteorder='big'))
print("Data Length:", int.from_bytes(data_len, byteorder='big'))
print("Checksum:", int.from_bytes(checksum, byteorder='big'))
print("Payload:", payload)


import unittest
import socket
import base64

class TestUDPServer(unittest.TestCase):
    
    def test_decode_packet(self):
        # create a UDP socket and bind it to a specific address
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_address = ('localhost', 1234)
        sock.bind(server_address)
        
        # send a packet to the socket
        payload = b'Welcome to IoT UDP Server'
        src_port = (10).to_bytes(2, byteorder='big')
        dest_port = (42).to_bytes(2, byteorder='big')
        data_len = len(payload).to_bytes(2, byteorder='big')
        packet = src_port + dest_port + data_len + b'\x00\x00' + payload
        encoded_packet = base64.b64encode(packet)
        sock.sendto(encoded_packet, server_address)
        
        # receive data from the socket
        data, addr = sock.recvfrom(1024)
        decoded_data = base64.b64decode(data)
        
        # check if the extracted fields match the expected values
        self.assertEqual(int.from_bytes(decoded_data[0:2], byteorder='big'), 10)
        self.assertEqual(int.from_bytes(decoded_data[2:4], byteorder='big'), 42)
        self.assertEqual(int.from_bytes(decoded_data[4:6], byteorder='big'), len(payload))
        self.assertEqual(decoded_data[6:8], b'\x00\x00')
        self.assertEqual(decoded_data[8:], payload)
        
if __name__ == '__main__':
    unittest.main()
